/* ENTRANCE POINT
   Author: Kesano
   Created: 02.06.2016
*/
 
//Config loading
config = require('./config');
md5 = require("md5");

// Arrays for service

connections = {}; 
clients = {};
urls = {};

// Modules
require('./modules/ws');
require('./modules/router');
require('./modules/url');
require('./modules/admin');

console.log("Server started on ports "+config.ws_port);
console.log("Server has started.");