//Websockets working file.
//Required by APP to create and serve WebSocket Server for users and Admin
  var ws = require("ws");
  
// Client Side to fill statistic 
  var ws_client = new ws.Server({port: config.ws_port});
  
  ws_client.on('connection', function(ws) {

  var connid = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 3) + new Date().getTime() ;
  connections[connid] = ws;
  console.log("New connection: " + connid);
  //console.log (ws.upgradeReq); 
  ws.on('message', function(message) {
	try { msg = JSON.parse(message);
		}
	catch(e){ return false;
		}
	Router(connid,msg);
	//console.dir(msg);	
  });

  ws.on('close', function() {
    console.log('Conn.close ' + connid);
	try {
		delete  urls[clients[connid].url].clients[connid];
		if(Object.keys(urls[clients[connid].url].clients).length==0) { 
			delete  urls[clients[connid].url]; 
		} 
	}catch(e){};
	try {delete  clients[connid]}catch(e){};
	try {delete  connections[connid]}catch(e){};
	console.log(urls);
  });

 });

 // Admin Side working on other port to separate connections
 
 var ws_admin = new ws.Server({port: config.admin_port});
  ws_admin.on('connection', function(ws) {
  var connid = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 3) + new Date().getTime() ;
  connections[connid] = ws;
  console.log("New connection: " + connid);
  ws.on('message', function(message) {
	try { msg = JSON.parse(message);
		}
	catch(e){ return false;
		}
	Router(connid,msg);
  });

  ws.on('close', function() {
    console.log('Admin leave ' + connid);
	try {delete  connections[connid]}catch(e){};
	console.log(urls);
  });

 });

//Safe message sending function
safe = {
	"send": function(connid,data) {
		if(connid in connections) {
			try { connections[connid].send(JSON.stringify(data));}catch(e) { console.log(e); }
		}
	}
}