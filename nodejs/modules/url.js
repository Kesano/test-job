/* Url Functions
   Author: Kesano
   02.06.2016
*/
url = {
	"new" : function(connid,data) {
		if("url" in data) {
			var now = Math.floor(Date.now() / 1000);
			var ip = connections[connid]._socket.remoteAddress;
			var urlcoded = md5(data.url);
			
			if(urlcoded in urls) {
				urls[urlcoded].clients[connid] = { ip:ip,time:now };
			}
			else {
				urls[urlcoded] = {};
				urls[urlcoded].url = data.url;
				urls[urlcoded].clients = {};
				urls[urlcoded].clients[connid] = { ip:ip,time:now };
			}
			clients[connid] = {};
			clients[connid].ip = ip;
			clients[connid].url = urlcoded;
			
			console.log(urls);
		}
	}
}