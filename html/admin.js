var socket;
window.onload = function() {

if (!window.WebSocket) {
	document.body.innerHTML = 'WebSocket в этом браузере не поддерживается.';
}
  socket = new WebSocket("ws://kx.proskura.com:11002");

	
// Incoming messages processing
socket.onmessage = function(event) {
  var incomingData = {};
  try { incomingData = JSON.parse(event.data);} catch(e) {}
  console.log(incomingData);
  if("type","action","data" in incomingData) {
	  switch(incomingData.type) {
		case "admin":
			switch(incomingData.action) {
				case "stat":
					if("pages" in incomingData.data){
						showStat(incomingData.data);
					}
				break;
			}
		break;

		default: break;
	  }
  }
};

};

//Safe message sending
safe = {
	"send": function(data) {
		if("readyState" in socket){
			if(socket.readyState === 1) {
				socket.send(JSON.stringify(data));
			}
			else if(socket.readyState === 0) {
				$('#pages').empty();
				$('#pages').html('Please wait for connection!');
			}
			else {
				$('#pages').empty();
				$('#pages').html('Connection has been interrupted! Reload the page!');
			}
		}
		else {
			//alert('Socket not created. Reload the page!');
		}
	}
};	


// Functions
function showStat(data) {
	$('#pages').empty();
	$('#pages').append('<div class="totalclients"><h2>Total Clients online: '+data["clients"]+' </h2></div>');
	$.each(data.pages, function(key,page){
		var clients = "";
		$.each(page.clients, function(key2,client){
			clients += '<div class="client"><div class="ip">'+client["ip"]+'</div><div class="time">'+(data.sysTime - client["time"])+'s</div></div>';
		});
		$('#pages').append('<div class="page"><div class="url">'+page["url"]+' (Visitors:'+(Object.keys(page.clients).length)+')</div>'+clients+'</div>');
	});	
}

setInterval(function(){ var i={"type":"admin","action":"stat","data":{}};safe.send(i);},1000);


