
window.onload = function() {

if (!window.WebSocket) {
	document.body.innerHTML = 'WebSocket в этом браузере не поддерживается.';
}
  socket = new WebSocket("ws://kx.proskura.com:11001");

socket.onopen = function(event) {
	$('#page').html('<h1>WS Connection opened</h1>');
	var data = {type:"url",action:"new",data:{url:window.location.href}};
	safe.send(data);
}


// обработчик входящих сообщений
socket.onmessage = function(event) {};

};
//Safe message sending
safe = {
	"send": function(data) {
		if("readyState" in socket){
			if(socket.readyState === 1) {
				socket.send(JSON.stringify(data));
			}
			else {
				alert('Connection is interrupted! Reload the page!');
			}
		}
		else {
			alert('Socket not created. Reload the page!');
		}
	}
};	
